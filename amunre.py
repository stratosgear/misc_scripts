import itertools

# https://math.stackexchange.com/questions/3922836/weird-pattern-combination-in-amun-re-card-game

cards = [0, 1, 2, 3, 4, 5, 6, 7, 8]

stats = {}
for sumof in range(1, 40):
    print(f"Sum of {sumof}:")
    opts = []
    for items in range(1, len(cards) + 1):
        for some_set in itertools.combinations(cards, items):
            if sum(some_set) == sumof and 0 in some_set:
                opts.append([some_set])

    opts = sorted(opts)
    for o in opts:
        print(f"  {o[0]}")

    for x in range(len(cards)):
        count = 0
        for o in opts:
            if x in o[0]:
                count += 1
        prev = stats.get(x, 0)
        stats[x] = prev + count

print("Stats of card participation in sets:")
print(stats)